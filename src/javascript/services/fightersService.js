import { callApi } from '../helpers/apiHelper';

class FighterService {
  async getFighters() {
    try {
      const endpoint = 'fighters.json';
      const apiResult = await callApi(endpoint, 'GET');

      return apiResult;
    } catch (error) {
      throw error;
    }
  }

  async getFighterDetails(id) {
    const endpointUrl = `details/fighter/${id}.json`;
    const apiResult = await callApi(endpointUrl, 'GET');
    return apiResult;
  }
}

export const fighterService = new FighterService();
