import {
  getCriticalDamage,
  getDamage,
} from './fight';

export class ArenaFighter {
  constructor(fighterDetails, position) {
    this.attack = fighterDetails.attack;
    this.defense = fighterDetails.defense;
    this.health = fighterDetails.health;
    this.initialHealth = fighterDetails.health;
    this.position = position;
    this.healthbar = document.getElementById(`${this.position}-fighter-indicator`);
    this.criticalHitAllowed = true;
    this.isBlocking = false;
  }

  get canHit() {
    return !this.isBlocking;
  }

  get canHitCritical() {
    return this.canHit && this.criticalHitAllowed;
  }

  hit(defender) {
    if (this.canHit) {
      const damage = defender.isBlocking ? 0 : getDamage(this, defender);
      defender.applyDamage(damage);
    }
  }

  hitCritical(defender) {
    const CRITICAL_HIT_RESTORE_TIME = 10000;
    const restoreCriticalHitAbility = () => {
      this.criticalHitAllowed = true;
    };
    if (this.canHitCritical) {
      const damage = getCriticalDamage(this);
      defender.applyDamage(damage);
      this.criticalHitAllowed = false;
      setTimeout(restoreCriticalHitAbility, CRITICAL_HIT_RESTORE_TIME);
    }
  }

  block() {
    this.isBlocking = true;
  }

  stopBlocking() {
    this.isBlocking = false;
  }

  applyDamage(damage) {
    this.health = Math.max(0, this.health - damage);
    this.redrawHealthbar();
  }

  redrawHealthbar() {
    const width = (this.health / this.initialHealth) * 100;
    this.healthbar.style.width = width + '%';
  }
}