import { createElement, createElementWithText } from '../../helpers/domHelper';
import { showModal } from './modal';

export function showWinnerModal(fighter) {
  const bodyElement = createElement({ tagName: 'div', className: 'modal-body' });
  const textElement = createElementWithText({ tagName: 'span' }, `${fighter.name} won!`);
  bodyElement.append(textElement);
  showModal({ title: 'Game over', bodyElement });
}
