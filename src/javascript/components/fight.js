import { controls } from '../../constants/controls';
import { ArenaFighter } from './arenaFighter';

export async function fight(firstFighterDetails, secondFighterDetails) {
  return new Promise(resolve => {
    const fighterOne = new ArenaFighter(firstFighterDetails, 'left');
    const fighterTwo = new ArenaFighter(secondFighterDetails, 'right');

    const pressedKeys = new Set();

    document.addEventListener('keydown', onKeyDown);
    document.addEventListener('keyup', onKeyUp);

    function onKeyDown(event) {
      if (event.repeat) return;
      pressedKeys.add(event.code);
      switch (event.code) {
        case controls.PlayerOneAttack: {
          fighterOne.hit(fighterTwo);
          break;
        }
        case controls.PlayerOneBlock: {
          fighterOne.block();
          break;
        }
        case controls.PlayerTwoAttack: {
          fighterTwo.hit(fighterOne);
          break;
        }
        case controls.PlayerTwoBlock: {
          fighterTwo.block();
          break;
        }
      }
      // handle combinations
      if (combinationPressed(controls.PlayerOneCriticalHitCombination)) {
        fighterOne.hitCritical(fighterTwo);
      }
      if (combinationPressed(controls.PlayerTwoCriticalHitCombination)) {
        fighterTwo.hitCritical(fighterOne);
      }

      if (isGameOver()) {
        const winner = fighterOne.health === 0 ?
          secondFighterDetails :
          firstFighterDetails;
        finishGame(winner);
      }
    }

    function isGameOver() {
      return fighterOne.health === 0 || fighterTwo.health === 0;
    }

    function combinationPressed(combination) {
      return combination.every(key => pressedKeys.has(key));
    }

    function onKeyUp(event) {
      pressedKeys.delete(event.code);
      if (event.code === controls.PlayerOneBlock) {
        fighterOne.stopBlocking();
      }
      if (event.code === controls.PlayerTwoBlock) {
        fighterTwo.stopBlocking();
      }
    }

    function finishGame(winner) {
      document.removeEventListener('keydown', onKeyDown);
      document.removeEventListener('keyup', onKeyDown);
      resolve(winner);
    }
  });
}

export function getCriticalDamage(fighter) {
  return fighter.attack * 2;
}

export function getDamage(attacker, defender) {
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return Math.max(0, damage);
}

export function getHitPower(fighter) {
  const criticalHitChance = getRandomNumber(1, 2);
  const hitPower = fighter.attack * criticalHitChance;
  return hitPower;
}

export function getBlockPower(fighter) {
  const dodgeChance = getRandomNumber(1, 2);
  const blockPower = fighter.defense * dodgeChance;
  return blockPower;
}

function getRandomNumber(min, max) {
  return Math.random() * (max - min) + min;
}